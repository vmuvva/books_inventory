namespace :books do
	desc "find the return date of books and send reminder email"
	task :find_run_jobs => :environment do
		check_date1 = (Time.now).strftime("%Y-%m-%d")
    check_date2 = (Time.now+7.day).strftime("%Y-%m-%d")
    reminders_this_week = Request.where("to_date >= '#{check_date1}' and to_date <= '#{check_date2}' and state = 'issued'")
    pending_reminders =  Request.where("to_date < '#{check_date1}' and state = 'issued'")
    reminders_this_week && reminders_this_week.each do |rtw|
      user_name, user_email = PramatiLdap::get_details(rtw.book.possessor)
      due_date = rtw.to_date
      BookMailer.book_return_reminder_email(user_name, user_email, rtw.book, "is on #{due_date.to_date.inspect}").deliver
    end
    pending_reminders && pending_reminders.each do |pr|
      user_name, user_email = PramatiLdap::get_details(pr.book.possessor)
      due_date = pr.to_date
      BookMailer.book_return_reminder_email(user_name, user_email, pr.book, "was on #{due_date.to_date.inspect}").deliver
    end
	end
end
