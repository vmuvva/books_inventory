module BooksHelper
  def device_actions(device)
    case device.state.to_sym
    when :available
      if !Request.has_max_req_reached?(current_user)
        link_to 'Request', '#device-actions', class: 'btn btn-mini btn-primary request-device', id: "request-device-#{device.id}"

      end
    when :in_use
        if(BookRequest.requests_in_queue(device.id, current_user).present?)
            form_tag("/books/#{device.id}/dequeue_request", :method => "put") do 
              submit_tag 'De Queue',  class: 'btn btn-mini btn-primary request-device-dequeue'    
            end             
        elsif(BookRequest.find_all_by_username(current_user).size < 2)       
          form_tag("/books/#{device.id}/queue_request", :method => "put") do 
            submit_tag '+Queue',  class: 'btn btn-mini btn-primary request-device-queue'    
        end unless (current_user == device.possessor)
        end
       
    when :waiting
      link_to(
        'Waiting', '#device-actions',
        :class => 'btn btn-mini waiting-device disabled', :id => "waiting-device-#{device.id}",
        'rel' => 'popover', 'data-content' => "This device has already been requested by `#{device.requests.pending.first.try(:requestor)}`",
        'data-original-title' => 'Waiting Approval'
      )
    when :not_available
      if is_admin?
        link_to 'Edit', edit_book_path(device)
      end
    end
  end
end
