class BookObserver < ActiveRecord::Observer
  observe :book, :request

  def before_receive(book, transition)    
    book.possessor = nil
    book.get_issued_request.try(:archive)
    
  end

  def after_approve(req, transition)    
    req.book.assign_to(req.requestor)
    begin
      BookMailer.approval_email(req.owner, req.requestor, req.book).deliver
    rescue      
    end
    Event.record_event(req.book_id, "Book has been issued to #{req.requestor}")
  end

  def after_reject(req, transition)
    req.book.deny
    Event.record_event(req.book_id, "Book has been rejected to #{req.requestor}")
  end

  def before_make_available(book, transition)
    #book.possessor = nil
    #book.get_issued_request.try(:archive)
  end

  def after_make_available(book, transition)
    if(!book.possessor.blank?)
      book.try(:ask!)
      book.try(:assign!)
    end
  end

  def before_make_unavailable(book, transition)
    #book.possessor = nil
    #book.get_issued_request.try(:archive)
  end
end
