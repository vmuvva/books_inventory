class BookRequest < ActiveRecord::Base
  attr_accessible :book_id, :username
  belongs_to :book

  validates :username, :book_id , presence: true

  def self.get_requests_by_book_id(book_id)
  	 BookRequest.find_all_by_book_id(book_id)
  end

  def self.requests_in_queue(book_id, username)
  	BookRequest.where({:book_id => book_id , :username =>username})
  end
end