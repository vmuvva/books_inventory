class Location < ActiveRecord::Base
  attr_accessible :location_name
  validates :location_name, uniqueness: true, presence: true  

end
