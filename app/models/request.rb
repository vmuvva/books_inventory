class Request < ActiveRecord::Base
  attr_accessible :book_id, :owner, :possessor, :from_date, :to_date

  attr_accessor :send_email
  # Associations
  belongs_to :book

  # Scopes
  scope :pending, where(state: :pending)

  # Validations
  validates :from_date, presence: true

  # Callback
  after_create :send_request_email, :unless => Proc.new{ self.send_email == false }

  # State Machine
  state_machine :state, :initial => :pending do
    event :reject do
      transition :pending => :archived
    end
    event :approve do
      transition :pending => :issued
    end
    event :archive do
      transition :issued => :archived
    end
  end

  def reject_with_reason(reason)
    self.reject
    BookMailer.rejection_email(self.owner, self.requestor, self.book, reason).deliver
  end
  
  
  def self.is_pending(requestor)
    where("requestor = ? and  state = ?", requestor , "pending")
  end
  

  def self.pending_count(username)
    admin = Administrator.find_by_username(username)
    if  admin.present?
       Request.joins(:book).pending.where("books.location_id = ?","#{admin.location.id}").size
    else
      0
    end
  end

  def self.has_max_req_reached?(username)
     Request.where("requestor like ? and  state  in (?, ?)", "#{username}", "pending", "issued" ).size >= 2
  end

  private

  def send_request_email    
    admins = Administrator.find_all_by_location_id(self.book.location_id).select{|admin| admin.is_request_mail_enabled == true}
    admins.each do |admin|
      BookMailer.request_email(admin.username, self.requestor, self.book).deliver
    end
  end

end
