class Event < ActiveRecord::Base
  attr_accessible :event_type, :book_id, :comments

  default_scope order('created_at DESC')

  paginates_per 20

  def self.record_event(book_id, message, comments = '')
    Event.create(:book_id => book_id, :event_type => message, :comments => comments)
  end
end
