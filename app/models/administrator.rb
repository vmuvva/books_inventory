class Administrator < ActiveRecord::Base
  attr_accessible :username, :location_id, :is_request_mail_enabled

  validate :location_id, :presence => true
  validates :username, uniqueness: true, presence: true
  belongs_to :location

  def self.get_by_location(location)
  	Administrator.find_all_by_location_id(location)
  end

end
