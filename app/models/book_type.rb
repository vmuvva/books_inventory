class BookType < ActiveRecord::Base
  attr_accessible :name

  validates :name, uniqueness: true, presence: true

  def self.get_book_type_id(book_type)
  	 BookType.find_by_name(book_type).try(:id)
  end
end
