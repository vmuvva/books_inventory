class Review < ActiveRecord::Base
  attr_accessible  :description, :book_id, :username
  validates :book_id, presence: true
  # Associations
  belongs_to :book
  

end
