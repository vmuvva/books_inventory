class EventsController < ApplicationController

  before_filter :login_required

  def index
    @book = Book.find(params[:book_id])
    @events = @book.events.page(params[:page] || 1)
  end

end
