class SessionsController < ApplicationController

  # GET /sign_in
  def new
  end

  # POST /sign_in
  def create
    if PramatiLdap::authenticate(params[:username], params[:password])
      session[:username] = params[:username]
      #session[:is_admin] = true
      flash.now[:alert] = 'Successfully signed in'
      redirect_to books_path
    else
      flash.now[:error] = 'Invalid username and password'
      render action: :new
    end
  end

  # DELETE /sign_out
  def destroy
    session[:username], session[:is_admin] = nil, nil
    flash[:alert] = 'You\'re logged out of the application'
    redirect_to signin_path
  end


   # POST /authenticate.json
  def authenticate
    respond_to do |format|
      if PramatiLdap::authenticate(params[:username], params[:password])
        format.json { render json: {:status => "success"}.to_json }
        format.xml  {render :xml => {:status => "success"}.to_xml(:root => 'authentication')}
      else
        format.json { render json: {:status => "failed"}.to_json }
        format.xml  {render :xml => {:status => "failed"}.to_xml(:root => 'authentication')}
      end
    end
  end

end

