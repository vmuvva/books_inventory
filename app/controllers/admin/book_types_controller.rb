class Admin::BookTypesController < ApplicationController
  before_filter :admin_required

  def create
    @book_type = BookType.new(params[:book_type])

    respond_to do |format|
      if @book_type.save
        format.html { redirect_to admin_dashboard_path, notice: 'Book Type was successfully created.' }
        format.json { render json: @book_type, status: :created, location: @book_type }
      else
        format.html { redirect_to admin_dashboard_path, notice: "Some errors prevented the book_type from saving" }
        format.json { render json: @book_type.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @book_type = BookType.find(params[:id])

    respond_to do |format|
      if @book_type.update_attributes(params[:book_type])
      else
      end
    end
  end
end
