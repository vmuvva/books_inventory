class Admin::DashboardController < ApplicationController
  before_filter :admin_required

  def index
    @administrators = Administrator.all
    @book_types = BookType.all
    #@accessory_types = AccessoryType.all

    @administrator = Administrator.new
    @book_type = BookType.new
   # @accessory_type = AccessoryType.new
  end
end
