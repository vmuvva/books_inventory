class ReviewsController < ApplicationController

	def index
	  @comments = Review.find_all_by_book_id(params[:book_id])
      respond_to do |format|
        format.html 
        format.json { render json: @comments }
      end
	end

	def create
	 @new_comment = Review.new(params[:review].merge!({:book_id => params[:book_id], :username=>current_user}))	 
	 @book = Book.find_by_id(params[:book_id])	
     respond_to do |format|
      	 @new_comment.save   
      	  @comments = Review.find_all_by_book_id(params[:book_id])
      	 @comment = Review.new    	
      	 format.js
	 end
	end

  def destroy
    @comment = Review.find(params[:id])
    @book = Book.find(params[:book_id])
    respond_to do |format|
      if @comment.destroy
        format.html { redirect_to book_path(@book), notice: 'Comment was successfully destroyed.' }
      else
        format.html { redirect_to book_path(@book), notice: "Some errors prevented the administrator from destroying" }
      end
    end
  end

end
