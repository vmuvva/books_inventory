class BooksController < ApplicationController
  before_filter :login_required, :unless => [:format_json?, :format_xml?]
  before_filter :admin_required, only: [:new, :create, :edit, :update, :destroy, :unavailable]
  
  def format_json?
    request.format.json?
  end
  
  def format_xml?
    request.format.xml?
  end
  
  # GET /book_types.json
    def book_types    
    @book_types = BookType.all #list_for(current_user)
    respond_to do |format|
      format.json { render json: @book_types }
      format.xml { render xml: @book_types } 
    end
  end
  
  
  # GET /books
  def index    
    @books = Book.all #list_for(current_user)
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @books } 
      format.xml {
         username = params[:username]
         if username
           books = Book.where('id not in (select book_id from book_requests where username = ?)', username).reject { |book| book.possessor == username}
           render xml: books
          else
            render xml: @books 
         end  
         }
    end
  end

  # GET /admin/books/unavailable
  def unavailable
    @books = Book.unavailable

    respond_to do |format|
      format.html
      format.json { render json: @books }
      format.xml { render xml: @books }
    end
  end
  
  # GET /admin/books/waiting.xml
  def my_books_waiting
    username= params[:username]
    @books = Book.in_wait(username)
    respond_to do |format|
      format.xml { render xml: @books.empty? ? {}.to_xml(:root => 'books') : @books }
    end
  end
  
 
  # Exporting books list to a Excel file
  def export
    @books = Book.all
    respond_to do |format|
      format.xls do
        workbook = Spreadsheet::Workbook.new
        worksheet = workbook.create_worksheet(name: 'Books List')
        worksheet.row(0).concat [
          'Title',
          'author',
          'Publication',
          'Condition',
          'Review Link',
          'Type',          
          'Location',
          'State',
          'Possessor',
          'Property Of'
         # 'Owner'
        ] 
        @books.each_with_index { |device, i|
          worksheet.row(i+1).push(
            device.title,
            device.author,
            device.publication,
            device.condition,            
            device.link_for_review,
            device.book_type.try(:name),   
            device.location.location_name,        
            device.state,            
            device.possessor,
            device.property_of
           # device.owner
          )
        }

 
        header_format = Spreadsheet::Format.new(color: :green, weight: :bold)
        worksheet.row(0).default_format = header_format
        #output to blob object
        blob = StringIO.new('')
        workbook.write(blob)
        #respond with blob object as a file
        send_data(blob.string, :type => "application/ms-excel", :filename => "Books List.xls")
      end
    end
  end
  
  #Import Books Data from Excel
  def import
    if params[:book_import_file]
      imp_filename = params[:book_import_file].tempfile.path
      Spreadsheet.open(imp_filename) do |spreadsheet|
        ws = spreadsheet.worksheets.first

        # No worksheets
        if ws.nil?
          flash[:error] = 'No worksheets in the Excel sheet provided'
        else
          ws.each do |row|
            d = Book.new
            #break if row[0].nil?           
            d.title            = row[1]
            d.author           = row[2]
            d.publication      = row[3]
            d.condition        = row[4]
            d.link_for_review  = row[5] 
            d.book_type_id     = BookType.get_book_type_id(row[6])            
            d.location_id      = Location.find_by_location_name(row[7])
            d.possessor        = row[8]
            d.property_of      = row[9]
           # d.owner            = row[9]
           
            d.state            = :available
            d.save!

            Event.record_event(d.id, "Book has been imported")
          end
          flash[:notice] = 'Import successful, new records added to the database.'
        end
      end
    else
      flash[:error] = 'No file uploaded'
    end

    respond_to do |format|
      format.html { redirect_to books_path }
    end
  end

  # GET /books/search
  def search
    @books = Book.search(params[:q])
    respond_to do |format|
      format.html # search.html.erb
      format.json { render json: @books }
      format.xml {
        username = params[:username]
        book_type = params[:category]
        book_type_id = BookType.where("lower(name) like  ?", book_type.downcase).first if book_type
        if username
          if book_type_id
            books = Book.search(params[:q]).where(' (book_type_id = ?) and (id not in (select book_id from book_requests where username = ?))', book_type_id, username).reject { |book| book.possessor == username}
          else
            books = Book.search(params[:q]).where('id not in (select book_id from book_requests where username = ?)', username).reject { |book| book.possessor == username}
          end
          render xml: books.empty? ? {}.to_xml(:root => 'books') : books
        else
          render xml: @books.empty? ? {}.to_xml(:root => 'books') : @books
        end
      }
    end
  end

  # GET /books/1
  def show
    @book = Book.find(params[:id])
    @events = @book.events.limit(10)
    @comments = Review.find_all_by_book_id(params[:id])
    @comment = Review.new    

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @book }
      format.xml { render xml: @book }
    end
  end

  # GET /books/new
  # GET /books/new.json
  def new
    @book = Book.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @book }
    end
  end

  # GET /books/1/edit
  def edit
    @book = Book.find(params[:id])

    respond_to do |format|
      format.html # edit.html.erb
      format.json { render json: @book }
    end
  end

  # POST /books
  # POST /books.json
  def create
    params[:book].merge!({:created_by => current_user})
    @book = Book.new(params[:book])

    respond_to do |format|
      if @book.save
        Event.record_event(@book.id, "book has been created")
        format.html { redirect_to @book, notice: 'Book was successfully created.' }
        format.json { render json: @book, status: :created, location: @book }
      else
        flash.now[:error] = "Some errors prevented the book from saving"
        format.html { render action: "new" }
        format.json { render json: @book.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /books/1
  def update
    @book = Book.find(params[:id])

    if params[:book][:book_possessor].present?
      params[:request] = {from_date: Date.today, to_date: (Date.today + 21.days)}
      make_request(params[:book][:book_possessor], false)
      @req.owner = current_user
      @req.approve            
    end
    params[:book].delete(:book_possessor)
    

    params[:book].merge!({:updated_by => current_user})
    respond_to do |format|
      if @book.update_attribs(params[:book], params[:comment])
        format.html { redirect_to @book, notice: 'Book was successfully updated.' }
        format.json { render json: @book, status: :updated, location: @book }
      else
        flash.now[:error] = "Some errors prevented the data from updating"
        format.html { render action: "edit" }
      end
    end
  end

  def make_request(requestor, send_email)
    ActiveRecord::Base.transaction do
      @book.ask!
      @req = @book.requests.build(params[:request]) do |req|
        req.owner     = @book.owner
        req.requestor = requestor
        req.send_email = send_email
      end

      @req.save!
      @asked = true
    end
  end

  # PUT /books/:id/ask.js
  def ask
    @book = Book.find(params[:id])
    make_request(current_user, true)
    @books = Book.all
    respond_to do |format|
      if @asked
        flash.now[:notice] = 'Sent a request successfully.'
        format.html { redirect_to books_path }
        format.json { render json: @book, status: :requested, location: @book }
      else
        flash.now[:error] = 'Unable to add a request.'
        format.html { redirect_to books_path }
        format.json { render json: @book, status: :request_failed, location: @book }
      end
    end
  end
  
   # POST /books/:id/ask_book.xml
   # GET /books/:id/ask_book.xml
    
   def ask_book
    @book = Book.find(params[:id])
    user_name = params[:username]
    from_date = Time.now
    to_date = Time.now + 21.days
    params[:request] =  {:from_date=> from_date.strftime('%d-%m-%Y'), 
                         :to_date=> to_date.strftime('%d-%m-%Y')}
    
    make_request(user_name, true)
    respond_to do |format|
      if @asked
        format.xml  {render :xml => {:status => "success"}.to_xml(:root => 'ask-book')}
      else
        format.xml  {render :xml => {:status => "failed"}.to_xml(:root => 'ask-book')}
      end
    end
  end 
    
  

  def queue_request
    @book = Book.find(params[:id])
    username = current_user
    username = params[:username] unless username
    book_request = BookRequest.new(:book_id => @book.id, :username => username)
    @books = Book.all
    if book_request.save
      respond_to do |format|
        format.html {redirect_to books_path }
        format.xml  {render :xml => {:status => "success"}.to_xml(:root => 'queue-request')}
      end
    else
      respond_to do |format|
        format.html { redirect_to books_path }
        format.xml  {render :xml => {:status => "failed"}.to_xml(:root => 'queue-request')}
      end
    end  
  end 

  def dequeue_request
    @book = Book.find(params[:id])
    username = current_user
    username = params[:username] unless username
    book_request = BookRequest.where(:book_id => @book.id, :username => username).try(:first)
    @books = Book.all
    if (book_request && (BookRequest.delete(book_request.id) == 1))
      respond_to do |format|
        format.html { redirect_to books_path }
        format.xml  {render :xml => {:status => "success"}.to_xml(:root => 'dequeue-request')}
      end
    else
      respond_to do |format|
        format.html { redirect_to books_path }
        format.xml  {render :xml => {:status => "failed"}.to_xml(:root => 'dequeue-request')}
      end
    end
  end 

  # PUT /books/:id/receive
  def receive
    @book = Book.find(params[:id])
    possessor = @book.possessor
    book_requests = BookRequest.find_all_by_book_id(@book.id, :order=>"book_id asc")

    respond_to do |format|

      if @book.receive
        Event.record_event(@book.id, "book has been returned")
        BookMailer.recieve_email(possessor, current_user, @book).deliver
        if(book_requests.present?)
           first_request  = book_requests.first
           params[:request] = {from_date: Date.today, to_date: (Date.today + 21.days)}          
           make_request(first_request.username, false)
           @req.owner = current_user
           @req.approve
           BookRequest.delete(first_request.id)            
        end
        format.html { redirect_to @book, notice: 'Book returned successfully. Book assigned to first in queue.' }
      else
        format.html { redirect_to @book, notice: 'Unable to receive the book' }
      end
    end
  end

  def make_unavailable
    @book = Book.find(params[:id])

    respond_to do |format|
      if @book.make_unavailable
        Event.record_event(@book.id, "Book has been made unavailable to other users")
        format.html { redirect_to edit_book_path, notice: 'The book is now marked unavailable to other users.' }
      else
        format.html { redirect_to edit_book_path, error: 'Unable to mark the book as unavailable.' }
      end
    end
  end

  def make_available
    @book = Book.find(params[:id])

    respond_to do |format|
      if @book.make_available
        Event.record_event(@book.id, "Book has been made available to other users")
        format.html { redirect_to edit_book_path, notice: 'The book is now marked available to other users.' }
      else
        format.html { redirect_to edit_book_path, error: 'Unable to mark the book as available.' }
      end
    end
  end

  # DELETE /books/1
  # DELETE /books/1.json
  def destroy
    @book = Book.find(params[:id])
    @book.destroy

    respond_to do |format|
      format.html { redirect_to books_url }
      format.json { head :no_content }
    end
  end
  
  # GET /my_books_current
  def my_books_current    
    @books = Book.all #list_for(current_user)
    username = params[:username]
    @books_availed = Book.where("possessor = ?", username)
    @books_in_queue = Book.where('id in (select book_id from book_requests where username = ?)', username)
    book_shelf_current = {"books-availed".to_sym => @books_availed, "books-in-queue".to_sym => @books_in_queue }
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @books }
      format.xml {render xml:  book_shelf_current.to_xml(:root => 'my-book-shelf-current', :children => 'book')}
    end
  end
  
  # GET /my_books_history
  def my_books_history    
    @books = Book.all #list_for(current_user)
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @books } 
      format.xml {render :xml => @books}
    end
  end
  
  
end
