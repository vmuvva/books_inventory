class RequestsController < ApplicationController
  before_filter :login_required 

  def index
    admin = Administrator.find_by_username(current_user)    
    @requests = Request.joins(:book).pending.where("books.location_id = ?","#{admin.location_id}")
  end

  def approve
    @req = Request.find(params[:id])
    @req.owner = current_user
    respond_to do |format|
      if @req.approve
        format.html { redirect_to requests_path, notice: "Successfully approved the request and an email has been sent to `#{@req.requestor}`" }
      else
        format.html { redirect_to requests_path, notice: "Unable to approve the request" }
      end
    end
  end

  def reject
    @req = Request.find(params[:id])
    @req.owner = current_user
    respond_to do |format|
      if @req.reject_with_reason(params[:reason])
        format.html { redirect_to requests_path, notice: "Successfully rejected the request" }
      else
        format.html { redirect_to requests_path, notice: "Unable to reject the request" }
      end
    end
  end
end
