class BookMailer < ActionMailer::Base
  add_template_helper(ApplicationHelper)
  default from: "books@pramati.com"

  def request_email(owner, requestor, book)
    @owner_name, @owner_email         = PramatiLdap::get_details(owner)
    @requestor_name, @requestor_email = PramatiLdap::get_details(requestor)
    @book                           = book

    mail(to: @owner_email, subject: "Request for #{@book.title}")
  end

  def recieve_email(requestor, owner,  book)    
    @requestor_name, @requestor_email = PramatiLdap::get_details(requestor)
    @owner_name, @owner_email         = PramatiLdap::get_details(owner)
    @book                             = book

    mail(to: @requestor_email, subject: "Received  #{@book.title}")
  end

  def approval_email(owner, requestor, book)
    @owner_name, @owner_email         = PramatiLdap::get_details(owner)
    @requestor_name, @requestor_email = PramatiLdap::get_details(requestor)
    @book                             = book

    mail(to: @requestor_email, subject: "Request for #{@book.title}")
  end
  
  def ownership_email(owner, requestor, book)
    @owner_name, @owner_email         = PramatiLdap::get_details(owner)
    @requestor_name, @requestor_email = PramatiLdap::get_details(requestor)
    @book                           = book

    mail(to: @requestor_email, subject: "Ownership Change :You are made owner to the book #{@book.title} by #{@owner_name}")
  end

  def rejection_email(owner, requestor, book, reason)
    @owner_name, @owner_email         = PramatiLdap::get_details(owner)
    @requestor_name, @requestor_email = PramatiLdap::get_details(requestor)
    @book                           = book
    @reason                           = reason

    mail(to: @requestor_email, subject: "Request for #{@book.title} rejected!")
  end

  def book_return_reminder_email(user_name, email, book, msg)
    @email = email
    @user_name = user_name
    @book = book
    @message = msg

    mail(to: @email, subject: "Reminder: #{@book.title} book to be returned")
  end

end
