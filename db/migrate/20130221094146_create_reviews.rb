class CreateReviews < ActiveRecord::Migration
  def up   
    create_table :reviews do |t|
      t.string  :description
      t.string  :ratings
      t.integer :book_id
      t.string  :username
      t.timestamps
    end 
  end

  def down
  end
end
