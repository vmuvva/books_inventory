class Events < ActiveRecord::Migration
  def up
  	create_table :events do |t|
      t.string  :event_type
      t.integer :book_id
      t.string :comments      
      t.timestamps
    end
  end

  def down
  end
end
