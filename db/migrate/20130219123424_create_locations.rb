class CreateLocations < ActiveRecord::Migration
  def up   
    create_table :locations do |t|
      t.string :desc
      t.string :location_name
      t.timestamps
    end 
  end

  def down
  end
end
