class BookTypes < ActiveRecord::Migration
  def up
  	create_table :book_types do |t|
      t.string :name
      t.timestamps
    end
  end

  def down
  end
end
