# encoding: utf-8
class CreateAdmins < ActiveRecord::Migration
  def up   
    create_table :administrators do |t|
      t.string :username
      t.integer :location_id
      t.boolean :is_request_mail_enabled
      t.timestamps
    end 
  end

  def down
  end
end
