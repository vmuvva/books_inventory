class Books < ActiveRecord::Migration
  def up
  create_table :books do |t|
      t.string :link_for_review
      t.string :title     
      t.string :publication      
      t.string :condition      
      t.string :phone_num
      t.string :state
      t.string :book_photo_file_name
      t.string :book_photo_content_type
      t.integer :book_photo_file_size
      t.integer :book_type_id      
      t.string :owner
      t.string :possessor
      t.string :property_of
      t.string :author
      t.string :created_by
      t.string :updated_by
      t.integer :location_id
      t.string :isbn
      t.string :image_link      

      t.timestamps
    end
  end

  def down
  end
end
