class Requests < ActiveRecord::Migration
  def up
  	create_table :requests do |t|
      t.string :book_id      
      t.string :owner
      t.string :requestor
      t.string :state
      t.date :from_date
      t.date :to_date      
      t.timestamps
    end
  end

  def down
  end
end
