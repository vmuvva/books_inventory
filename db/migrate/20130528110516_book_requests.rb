class BookRequests < ActiveRecord::Migration
  def up
  	create_table :book_requests do |t|      
      t.integer :book_id
      t.string  :username
      t.timestamps
    end 
  end

  def down
  end
end
