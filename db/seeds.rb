# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

p "Loading seed data from db/defaults.yml"
SEED_DATA = YAML.load_file("#{Rails.root}/db/defaults.yml")



p "Seeding table `device_types`"
SEED_DATA['book_types'].each do |book_type|
  BookType.create(name: book_type)
end


SEED_DATA['locations'].each do |book_type|
  Location.create(location_name: book_type)
end


