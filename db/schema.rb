# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130528110516) do

  create_table "administrators", :force => true do |t|
    t.string   "username"
    t.integer  "location_id"
    t.boolean  "is_request_mail_enabled"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "book_requests", :force => true do |t|
    t.integer  "book_id"
    t.string   "username"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "book_types", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "books", :force => true do |t|
    t.string   "link_for_review"
    t.string   "title"
    t.string   "publication"
    t.string   "condition"
    t.string   "phone_num"
    t.string   "state"
    t.string   "book_photo_file_name"
    t.string   "book_photo_content_type"
    t.integer  "book_photo_file_size"
    t.integer  "book_type_id"
    t.string   "owner"
    t.string   "possessor"
    t.string   "property_of"
    t.string   "author"
    t.string   "created_by"
    t.string   "updated_by"
    t.integer  "location_id"
    t.string   "isbn"
    t.string   "image_link"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "events", :force => true do |t|
    t.string   "event_type"
    t.integer  "book_id"
    t.string   "comments"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "locations", :force => true do |t|
    t.string   "desc"
    t.string   "location_name"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "requests", :force => true do |t|
    t.string   "book_id"
    t.string   "owner"
    t.string   "requestor"
    t.string   "state"
    t.date     "from_date"
    t.date     "to_date"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "reviews", :force => true do |t|
    t.string   "description"
    t.string   "ratings"
    t.integer  "book_id"
    t.string   "username"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

end
