DeviceInventory::Application.routes.draw do

  get '/signin' => 'sessions#new'
  delete'/signout' => 'sessions#destroy'

  resources :books do
    collection do
      get :search
      get :export
      get :unavailable
      post :import
      get :my_books_current
      get :my_books_history
      get :my_books_waiting
    end

    member do
      put :ask
      put :receive
      put :make_unavailable
      put :make_available
      put :queue_request
      put :dequeue_request
      put  :ask_book
      # added for the sake of xml api easy testing
      # and can be removed later
      post :ask_book
      get  :ask_book
      post :queue_request
      get  :queue_request
      post :dequeue_request
      get  :dequeue_request
      
    end

    resources :accessories
    resources :reviews
    resources :events, only: [:index]
  end

  resources :users do
    collection do
      get :search
    end
  end

  resources :requests, only: [:index] do
    member do
      put :approve
      put :reject
    end
  end

  resources :sessions

  namespace :admin do
    get 'dashboard' => 'dashboard#index'
    resources :book_types
    #resources :accessory_types
    resources :administrators do
      collection do
        get :search
      end
    end
  end
  
  match 'book_types' => 'books#book_types'
  match 'authentication' => 'sessions#authenticate'

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => 'books#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
